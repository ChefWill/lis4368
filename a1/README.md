# LIS4368 

## William Walker

### Assignment 1 Requirements:

README.md file should includethe following items:

1. Screenshotof running java Hello(#1 above);

2. Screenshotof running http://localhost:9999(#2above, Step #4(b)in tutorial);

3. Link to local lis4368 web app: http://localhost:9999/lis4368/

4. git commands w/short descriptions;

5. Bitbucket repo links:a.This assignment,and b.The completed tutorial repo above (bitbucketstationlocations). (See link in screenshot below.)

#### Git commands w/short descriptions:


1. git commit. The "commit" command is used to save your changes to the local repository. Note that you have to explicitly tell Git which changes you want to include in a commit before running the "git commit" command. This means that a file won't be automatically included in the next commit just because it was changed.

2. The git init command creates a new Git repository. It can be used to convert an existing, unversioned project to a Git repository or initialize a new, empty repository. Most other Git commands are not available outside of an initialized repository, so this is usually the first command you'll run in a new project.

3. git status. The git status command displays the state of the working directory and the staging area.

4. git push. The git push command is used to upload local repository content to a remote repository.

5. git pull is a Git command used to update the local version of a repository from a remote. 

6. A Git remote is kind of like a backup that is stored on someone else's computer. 

7. The git config command is a convenience function that is used to set Git configuration values on a global or local project level.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](/img/helloworld.png)

*Screenshot of Tomcat*
![Tomcat Installation](img/tomcat.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ChefWill/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
