-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema wlw16b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `wlw16b` ;

-- -----------------------------------------------------
-- Schema wlw16b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wlw16b` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `wlw16b` ;

-- -----------------------------------------------------
-- Table `wlw16b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wlw16b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `wlw16b`.`customer` (
  `cus_id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `cus_phone` BIGINT(20) NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`cus_id`),
  UNIQUE INDEX `cus_id_UNIQUE` (`cus_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `wlw16b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wlw16b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `wlw16b`.`petstore` (
  `pst_id` SMALLINT(6) NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED ZEROFILL NOT NULL,
  `pst_phone` BIGINT(20) NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`pst_id`),
  UNIQUE INDEX `pst_id_UNIQUE` (`pst_id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `wlw16b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `wlw16b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `wlw16b`.`pet` (
  `pet_id` SMALLINT(6) NOT NULL,
  `pst_id` SMALLINT(6) NOT NULL,
  `cus_id` SMALLINT(6) NULL DEFAULT NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT(4) NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`pet_id`),
  UNIQUE INDEX `pet_id_UNIQUE` (`pet_id` ASC),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `wlw16b`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `wlw16b`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
