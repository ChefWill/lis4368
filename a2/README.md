

# LIS 4368

## William Walker

### Assignment 2 Requirements:

*Sub-Heading:*

1. Show version control mastery
2. Show developmental installation success
3. Chapter questions

#### README.md file should include the following items:

* Screenshot of Tomcat hello page
* Screenshot of Tomcat index page
* Screenshot of Querybook




#### Assignment Screenshots:
http://localhost:9999/hello/querybook.html
![Bookshop](img/bookshop.png)

http://localhost:9999/hello/sayhello
![JDK Installation Screenshot](img/helloWorld.png)

*Screenshot of My Home*:
http://localhost:9999/hello/
![Home](img/willHome.png)

http://localhost:9999/hello/sayhi
![Hello Again](img/helloagain.png)
