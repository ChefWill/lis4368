> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368

## William Walker

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/ChefWill/lis4368/src/master/a1/ "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/ChefWill/lis4368/src/master/a2/ "My A2 README.md file")
    - Create index.html
    - Create querybook.html
    - Compile HelloServlet.java
    - Compile AnotherHelloServlet.java
    - Compile QueryServlet.java

3. [A3 README.md](https://bitbucket.org/ChefWill/lis4368/src/master/a3/README.md)

    - Entity Relationship Diagram (ERD)
    - Include dada (at least 10 records for each table)
    - Provide Bitbucket read-only access to repo must include README.md using markdown syntax and include links to all of the following files from README.md:
        - docs folder: a3.mwb and a3.sql
        - img folder: a3.png (export a3.mwb file as a a3.png)
        - README.md (*MUST* display a3.png ERD)

 4. [P1 README.md](https://bitbucket.org/ChefWill/lis4368/src/master/p1/README.md)       

    - Basic client-side validation
    - Show failed validation entry
    - Show successful validation entry
