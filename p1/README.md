
Project #1 Requirements:

Deliverables:


1. Desing a web application that validates data with regular expressions

README.md file should include the following items:

- Screenshot of P1 Failed Validation
- Screenshot of P1 Successful Validation

Project Screenshot and Links:

Screenshot of Failed Validation

![P1 Failed Validation](img/FailedValid.png "Failed Validation")


Screenshot of Successful Validation

![P1 Successful](img/SuccessValid.png "Successful Validation")

Links:
Web Portfolio for P1: http://localhost:9999/lis4368/p1/index.jsp